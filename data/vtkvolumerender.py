#!/Users/kmorel/local/bin/vtkpython

import vtk

filename='/Users/kmorel/data/supernova/normal_1349.nhdr'

print('Reading {}'.format(filename))
reader = vtk.vtkNrrdReader()
reader.SetFileName(filename)
reader.Update()

print reader.GetOutput().GetPointData().GetScalars().GetRange()

mapper = vtk.vtkFixedPointVolumeRayCastMapper()
mapper.SetInputConnection(reader.GetOutputPort())
mapper.SetSampleDistance(0.5)

volumeProperty = vtk.vtkVolumeProperty()
volumeProperty.ShadeOff()

opacity = vtk.vtkPiecewiseFunction()
opacity.AddPoint(0.0, 0.0)
opacity.AddPoint(0.114298, 1.0)
volumeProperty.SetScalarOpacity(opacity)

color = vtk.vtkColorTransferFunction()
color.AddRGBPoint(0.0, 85.0/255.0, 72.0/255.0, 193.0/255.0)
color.AddRGBPoint(0.114298/2.0, 221.0/255.0, 221.0/255.0, 221.0/255.0)
color.AddRGBPoint(0.114298, 177.0/255.0, 1.0/255.0, 39.0/255.0)
volumeProperty.SetColor(color)

volume = vtk.vtkVolume()
volume.SetMapper(mapper)
volume.SetProperty(volumeProperty)

renderer = vtk.vtkRenderer()
renderer.AddViewProp(volume)

window = vtk.vtkRenderWindow()
window.SetSize(1024, 1024)
window.AddRenderer(renderer)

renderer.ResetCamera()
camera = renderer.GetActiveCamera()
camera.Dolly(2.2)
renderer.ResetCameraClippingRange()

print 'Starting renders.'

window.Render()
window.Render()

timerlog = vtk.vtkTimerLog()

def StartRender(obj, ev):
    timerlog.MarkStartEvent('Render Start')

def EndRender(obj, ev):
    timerlog.MarkEndEvent('Render End')

mapper.AddObserver('VolumeMapperRenderStartEvent', StartRender, 0)
mapper.AddObserver('VolumeMapperRenderEndEvent', EndRender, 0)

if False:
    interactor = vtk.vtkRenderWindowInteractor()
    interactor.SetRenderWindow(window)
    interactor.Start()
else:
    for i in xrange(0,16):
        window.Render()

timerlog.DumpLog('timer.csv')
